///<reference path="../node_modules/angular2/typings/browser.d.ts"/>

import 'angular2/bundles/angular2-polyfills';
import { Component } from 'angular2/core';
import { bootstrap } from 'angular2/platform/browser';
import { Calendar, OrgChart } from 'oib-core-components/core';

@Component({
  selector: 'app',
  directives: [ Calendar, OrgChart ],
  template: `
    <div>
      <oib-calendar></oib-calendar>
      <oib-org-chart></oib-org-chart>
    </div>
  `
})
export class App {
}

bootstrap(App);