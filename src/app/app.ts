

import 'angular2/bundles/angular2-polyfills';

import { Component } from 'angular2/core';
import { bootstrap } from 'angular2/platform/browser';
import { Calendar } from '../calendar/calendar';
import { OrgChart } from '../orgChart/orgChart';

@Component({
  selector: 'app',
  directives: [ Calendar, OrgChart ],
  template: `
    <div>
      <oib-calendar></oib-calendar>
      <oib-org-chart></oib-org-chart>
    </div>
  `
})
export class App {
}

bootstrap(App);